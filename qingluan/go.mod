module qingluan

go 1.13

require (
	github.com/Microsoft/go-winio v0.5.1 // indirect
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf // indirect
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-plugins-helpers v0.0.0-20211224144127-6eecb7beb651
	golang.org/x/net v0.0.0-20211216030914-fe4d6282115f // indirect
	ovs v0.0.0
	db  v0.0.0
	iptables v0.0.0
)

replace ovs v0.0.0 => ../openvswitch/ovs
replace iptables v0.0.0 => ../iptables
replace db v0.0.0 => ../db
