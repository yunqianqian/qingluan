package ipamdriver

import (
	"sync"
)


type Queue struct {
	Items  [] string
	mutex  sync.Mutex
}

func (Q *Queue) Push(ele string) {
	Q.mutex.Lock()
	defer Q.mutex.Unlock()
	Q.Items = append(Q.Items, ele)
}

func (Q *Queue) Pop() string {
	Q.mutex.Lock()
	defer Q.mutex.Unlock()
	ele := Q.Items[0]
	Q.Items = Q.Items[1:]
	return ele
}

func (Q *Queue) IsEmpty() bool {
	Q.mutex.Lock()
	defer Q.mutex.Unlock()
	return len(Q.Items) == 0	
}

func (Q *Queue) Size() int {
	Q.mutex.Lock()
	defer Q.mutex.Unlock()
	return len(Q.Items)
}

