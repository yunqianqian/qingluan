package main

import (
	"sync"
	"fmt"
)


type Queue struct {
	Items  [] string
	Mutex  sync.Mutex
}

func (Q *Queue) Push (ele string) {
	Q.Mutex.Lock()
	defer Q.Mutex.Unlock()
	Q.Items = append(Q.Items, ele)
}

func (Q *Queue) Pop() string {
	Q.Mutex.Lock()
	defer Q.Mutex.Unlock()
	ele := Q.Items[0]
	Q.Items = Q.Items[1:]
	return ele
}

func (Q *Queue) IsEmpty() bool {
	Q.Mutex.Lock()
	defer Q.Mutex.Unlock()
	return len(Q.Items) == 0	
}

func (Q *Queue) Size() int {
	Q.Mutex.Lock()
	defer Q.Mutex.Unlock()
	return len(Q.Items)
}
