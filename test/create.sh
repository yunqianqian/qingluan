#!/bin/bash
docker network create \
        --driver qingluan \
        --ipam-driver qingluanIpam \
        --opt model=nat \
        --ipam-opt pool=nat \
        net1

docker network create \
        --driver qingluan \
        --ipam-driver qingluanIpam \
        --opt model=nat \
        --ipam-opt pool=nat \
        --subnet  192.168.1.0/24 \
        --gateway 192.168.1.1 \
        net2

docker network create \
        --driver qingluan \
        --ipam-driver qingluanIpam \
        --opt model=nat \
        --ipam-opt pool=nat \
        --subnet  192.168.1.0/24 \
        net3

docker network create \
        --driver qingluan \
        --ipam-driver qingluanIpam \
        --opt model=nonat \
        --ipam-opt pool=nonat \
        net4

docker network create \
        --driver qingluan \
        --ipam-driver qingluanIpam \
        --opt model=nonat \
        --ipam-opt pool=nonat \
        --subnet  192.168.2.0/24 \
        --gateway 192.168.2.1 \
        net5

    
docker network create \
        --driver qingluan \
        --ipam-driver qingluanIpam \
        --opt model=nonat \
        --ipam-opt pool=nonat \
        --subnet  192.168.2.0/24 \
        net6

docker network create \
        --driver qingluan \
        --ipam-driver qingluanIpam \
        --opt model=flat \
        --ipam-opt pool=flat \
        --subnet  192.168.159.0/24 \
        --gateway 192.168.159.1 \
        net7



docker network create \
        --driver qingluan \
        --ipam-driver qingluanIpam \
        --opt model=vlan \
        --opt vni=2 \
        --ipam-opt pool=vlan \
        --ipam-opt vni=2 \
        net8

docker network create \
        --driver qingluan \
        --ipam-driver qingluanIpam \
        --opt model=vlan \
        --opt vni=7 \
        --ipam-opt pool=vlan \
        --ipam-opt vni=7 \
        --subnet 192.168.1.0/24 \
        --gateway 192.168.1.1 \
        net9

docker network create \
        --driver qingluan \
        --ipam-driver qingluanIpam \
        --opt model=vlan \
        --opt vni=7 \
        --ipam-opt pool=vlan \
        --ipam-opt vni=7 \
        --subnet 192.168.1.0/24 \
        net10

