module qingluan_network_plugin

go 1.13

require (
	command v0.0.0
	github.com/docker/go-plugins-helpers v0.0.0-20211224144127-6eecb7beb651
	github.com/urfave/cli v1.22.5
	github.com/vishvananda/netlink v1.1.0 // indirect
	ipamdriver v0.0.0
	qingluan v0.0.0
)

replace (
	command v0.0.0 => ./command
	db v0.0.0 => ./db
	github.com/Sirupsen/logrus v1.8.1 => github.com/sirupsen/logrus v1.8.1
	github.com/coreos/bbolt v1.3.6 => go.etcd.io/bbolt v1.3.6
	github.com/sirupsen/logrus v1.8.1 => github.com/Sirupsen/logrus v1.8.1
	google.golang.org/grpc v1.43.0 => google.golang.org/grpc v1.26.0
	ipamdriver v0.0.0 => ./ipamdriver

	iptables v0.0.0 => ./iptables
	ovs v0.0.0 => ./openvswitch/ovs
	qingluan v0.0.0 => ./qingluan
	utils v0.0.0 => ./utils

)

require (
	9fans.net/go v0.0.4 // indirect
	github.com/acroca/go-symbols v0.1.1 // indirect
	github.com/cweill/gotests v1.6.0 // indirect
	github.com/davidrjenni/reftools v0.0.0-20210213085015-40322ffdc2e4 // indirect
	github.com/fatih/gomodifytags v1.16.0 // indirect
	github.com/haya14busa/goplay v1.0.0 // indirect
	github.com/josharian/impl v1.1.0 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/mdempsky/gocode v0.0.0-20200405233807-4acdcbdea79d // indirect
	github.com/ramya-rao-a/go-outline v0.0.0-20210608161538-9736a4bde949 // indirect
	github.com/rogpeppe/godef v1.1.2 // indirect
	github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966 // indirect
	github.com/sqs/goreturns v0.0.0-20181028201513-538ac6014518 // indirect
	github.com/yuin/goldmark v1.4.4 // indirect
	github.com/zmb3/gogetdoc v0.0.0-20190228002656-b37376c5da6a // indirect
	go.etcd.io/bbolt v1.3.6 // indirect
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616 // indirect
	golang.org/x/net v0.0.0-20220111093109-d55c255bac03 // indirect
	golang.org/x/sys v0.0.0-20220111092808-5a964db01320 // indirect
	golang.org/x/tools v0.1.8 // indirect
	sourcegraph.com/sqs/goreturns v0.0.0-20181028201513-538ac6014518 // indirect

)

replace (
	go.etcd.io/bbolt v1.3.4 => github.com/coreos/bbolt v1.3.4
	go.etcd.io/bbolt v1.3.6 => github.com/coreos/bbolt v1.3.6
)
