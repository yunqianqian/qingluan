module command

go 1.13

require db v0.0.0

require ovs v0.0.0

require utils v0.0.0

replace (
	db v0.0.0 => ../command
	ipamdriver v0.0.0 => ../ipamdriver
	ovs v0.0.0 => ../openvswitch/ovs
	utils v0.0.0 => ../utils
)
