### 实现过程中遇到的问题以及解决办法

#### 1. 导入etcd的库 "go.etcd.io/etcd/clientv3"出现依赖报错问题

```shell
replace (
	github.com/coreos/bbolt v1.3.6 => go.etcd.io/bbolt v1.3.6
	google.golang.org/grpc v1.43.0 => google.golang.org/grpc v1.26.0
)
```

#### 2.使用" github.com/urfave/cli" 库时候输入参数和实际输出不对应

问题出在bool类型的flag，bool类型直接写参数，不用写 true/flase

```go
        ...
        cli.BoolFlag{
            Name:        "debug",
            Usage:       "debug mode",
            Destination: &debug,
        },
        ...
// 运行的时候,bool类型不需要写true/false.
go run main.go --debug 

```

#### 3.etcd常用命令

插入数据

```shell
etcdctl put <key> <value>
```

按key值查找

```shell
etcdctl get <key>
```

不显示key只限制values

```shell
etcdctl get --print-value-only <key>
```

安装key的前缀查找

```shell
etcdctl get --prefix <key>
```

https://www.cnblogs.com/ilifeilong/p/11606757.html


#### 4.struct类型数据插入etcd

```go

// 属性元素要大写
type stu struct {
    ID     string
    Name    string
}

data, err := json.Marshal(config)
if err != nil {
    return err
}
if err := db.SetKey(configPrefix, string(data)); err != nil {
    return err
}	

```


