### 1. 启动参数问题
   system 管理，编写system 管理文件，传入一些参数，比如说网卡，然后就绑定好。

### 2. 4中network pool中都是PREFIX都是16
   规定网络池都是16位掩码，这样创建网络直接可以分出24的掩码。一方面简化处理、一方面强迫症。

### 3. ETCD中Key分类

总的索引
   qingluan

启动的时候Init配置
    qingluan/config


网络池索引
   qingluan/nat/info
   {
      "Pool" : "10.0.0.0/16",
      "Current" : "0",
      "Subpool": {
         "ID":       "11-11-11-11",
         "Network":  "10.0.1.0/24",
         "Gateway":  "10.0.0.1"
      }
   }

### 4. 业务口是单独的网卡
   <img src="https://gitee.com/yunqianqian/qingluan/raw/master/images/network.jpg"/>

  业务口是不需要IP的只需要



### 5. Flat网络池是不行的

### 6. Flat与其他网络创建时候IPAM区别
flat网络特殊，address全部都是由用户提供的。