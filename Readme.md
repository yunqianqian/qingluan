# 青鸾网络插件

## 青鸾介绍

青鸾出自山海经，是常伴西王母的一种神鸟，多为神仙坐骑。赤色多者为凤，青色多者为鸾，故唤作青鸾。

<img src="https://gitee.com/yunqianqian/qingluan/raw/master/images/qingluan.jpg" style="zoom: 40%;" />

qingluan(青鸾)是一款面向Docker网络集群的网络插件，主要是解决的是采用Docker组网而网络模式不足的问题。本插件目前支持5中网络模式分别是NONat模式、Nat模式、Flat模式、Vlan模式、Vxlan模式。辅助功能包括流量调度、流量监控、ACL控制以及集成入侵检测。其中NONat和Nat模式是Open vSwitch单机桥模式，Flat、Vlan、Vxlan都是支持跨主机通信的模式。


## 网络模式


### 桥模式
桥模式是类似于Docker原生的docker0网桥，但是青鸾实现了两种桥模式，NAT桥模式和NONAT桥模式。


#### NAT桥模式
NAT桥模式就和docker0网桥的功能一致，通过iptables实现NAT转换。
通过资源池分配网络的命令

```shell
docker network create \
        --driver qingluan \
        --ipam-driver qingluanIpam \
        --opt model=nat  \
        --ipam-opt pool=nat \
        nat1
```
通过自定义方法分配网络的命令,自定义网络范围，需要给定subnet的值，gateway可以指定也可以不指定，如果不指定，默认是xx.xx.xx.1是网关。
```shell
docker network create \
        --driver qingluan \
        --ipam-driver qingluanIpam \
        --opt model=nat \
        --ipam-opt pool=nat \
        --subnet  192.168.2.0/24 \
        --gateway 192.168.2.1 \
        nat2
```

NAT模式网络的网络和docker的docker0网络一样，会通过iptables做NAT转发以及端口映射。不过和docker不一样的是，NAT模式，没有docker-proxy进程。启动容器命令

```shell
docker run -itd \ 
	   --name busybox-nat \
	   --net nat1 \
       busybox:latest
```

带端口映射的容器，启动命令

```shell
docker run -itd \
       --name nginx-nat \
       --net nat1 \
       -p 1080:80 \
       nginx:latest
```

#### NONAT桥模式

NONAT桥模式就是本地的OVS网桥，除了虚拟机的网卡，不连接其他的网络设备，实际场景使用较少。类似于Neutron的local网络。
通过资源池分配网络的命令
```shell
docker network create \
        --driver qingluan \
        --ipam-driver qingluanIpam \
        --opt model=nonat \
        --ipam-opt pool=nonat \
        nonat1
```
通过自定义方法分配网络的命令,自定义网络范围，需要给定subnet的值，gateway可以指定也可以不指定，如果不指定，默认是xx.xx.xx.1是网关。
```shell
docker network create \
        --driver qingluan \
        --ipam-driver qingluanIpam \
        --opt model=nonat \
        --ipam-opt pool=nonat \
        --subnet  192.168.1.0/24 \
        --gateway 192.168.1.1 \
        nonat2
```
NONAT模式设计主机是内网中使用，而且是为不需要暴露端口的服务提供的网络。启动命令

```shell
docker run -itd 
	--name busybox-nonat \
        --net nonat1 \
       busybox:latest
```

### FLAT模式

不带vlan tag的网络，相当于NONAT桥网络的OVS连接到一个物理网卡。FLAT模式主机和宿主机都连接在一个二层网络中，会占用宿主机的IP资源。
和其他一些云平台扁平网络不同的是，青鸾网络插件虽然支持一个网络可以创建多个网络池子一样的FLAT网络，但是推荐一个网卡还是创建一个FLAT。
创建flat模式只能通过自定义网络范围来创建。注意创建flat模式需要指定参数subnet的值和gateway的值。
```shell
docker network create \
        --driver qingluan \
        --ipam-driver qingluanIpam \
        --opt model=flat \
        --ipam-opt pool=flat \
        --subnet  192.168.159.0/24 \
        --gateway 192.168.159.1 \
        net-name
```

### VLAN模式

基于物理Vlan网络实现，共享同一个物理网络的多个Vlan网络是相互隔离的，每个支持VLAN network的物理网络可以被视为一个分离的VLAN trunk，使用一组独占的vlan id。（有效段为1~4096）。VLAN跨主机通信需要外部的网关，以及需要配置成TRUNK模式。对于云主机操作不友好。
其中VLAN ID 的范围是 1 - 4094
通过资源池分配网络的命令
```shell
docker network create \
        --driver qingluan \
        --ipam-driver qingluanIpam \
        --opt model=vlan \
        --opt vni=<VLANID> \
        --ipam-opt pool=vlan \
        --ipam-opt vni=<VLANID> \
        net-name
```
通过自定义方法分配网络的命令,自定义网络范围，需要给定subnet的值，gateway可以指定也可以不指定，如果不指定，默认是xx.xx.xx.1是网关。
```shell
docker network create \
        --driver qingluan \
        --ipam-driver qingluanIpam \
        --opt model=vlan \
        --opt vni=<VLANID> \
        --ipam-opt pool=vlan \
        --ipam-opt vni=<VLANID> \
        --subnet 192.168.1.0/24 \
        --gateway 192.168.1.1 \
        net-name
```

### VXLAN模式

基于隧道技术的 overlay 网络，通过唯一的 VNI 区分于其他的 vxlan 网络，不和具体的物理网络绑定。在vxlan中，数据包通过 VNI 封装成UDP包进行传输，因为二层的包通过封装在三层传输，能够克服vlan和物理网络基础设施的限制。
暂未实现





## 辅助功能

### 流量调度

### 流量监控

### ACL控制

### 入侵检测



## 不足与更新

- iptables包写的比较混乱，需要优化下
- 创建容器的时候不要指定IP，除非你能每次不重复，这里也得优化下
- 批量删除容器`docker stop $(docker ps -q)`，会出现IP丢失的情况，循环单个删除的话，就不会。

