package main

import (
	"fmt"
	"os"

	"qingluan"
	"command"
	"ipamdriver"

	"github.com/urfave/cli"
	"github.com/docker/go-plugins-helpers/network"
	ipamapi "github.com/docker/go-plugins-helpers/ipam"
)

func main(){
	app := cli.NewApp()
	app.Name = "qingluan"
	app.Usage = "docker network plugin to simplify self-organizing networking"
	app.Author = "yunqianqian"
	app.Email = "369540029@qq.com"
	app.Version = "1.0.0"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "etcd",
			Value:       "127.0.0.1:2379",      
			Usage:       "ETCD store address",       
		},
		cli.StringFlag{
			Name:        "etcdprefix",
			Value:       "qingluan",      
			Usage:       "etcd perfix value",
		},
		cli.StringFlag{
			Name:        "nic",
			Value:       "",
			Usage:       "Flat and vlan bond nic",
		},
		cli.BoolFlag{
			Name:        "debug",
			Usage:       "Debug mode",
		},
		cli.BoolFlag{
			Name: 		"turnofffv",
			Usage:		"Turn off flat and vlan feature",
		},
		cli.StringFlag{
			Name:        "vlanpool",
			Value:       "10.2.0.1/16",
			Usage:       "Vlan pool address",         
		},
		cli.StringFlag{
			Name:        "vxlanpool",
			Value:       "10.3.0.1/16",
			Usage:       "Vxlan pool address",
		},
		cli.StringFlag{
			Name:        "natpool",
			Value:       "10.1.0.1/16",
			Usage:       "Nat pool address",         
		},
		cli.StringFlag{
			Name:        "nonatpool",
			Value:       "10.0.0.1/16",
			Usage:       "NoNAT pool address",
		},
	}

	app.Action = Run
	app.Run(os.Args)
}

func Run(c *cli.Context) {
	// turn off the flat and vlan feature
	if !c.Bool("turnofffv"){
		if c.String("nic") == "" {
			fmt.Println("please set nic parameters value. eg: ens33")
			os.Exit(1)			
		}else{
			command.InitNic(c.String("nic"))
		}
	}
	// set log level
	if c.Bool("debug") {
		command.InitializeLog(c.Bool("debug"))
	}
	// must provide etcd store ip parameters
	if c.String("etcd") == "" {
		fmt.Println("please set etcd store ip value. eg: 192.168.1.10:2376")
		os.Exit(1)
	}else {
		command.InitEtcd(c.String("etcd"), c.String("etcdprefix"))
	}
	pool := map[string]string{
		"nonat":  c.String("nonatpool"),
		"nat":    c.String("natpool"),
		"vlan":   c.String("vlanpool"),
		"vxlan":  c.String("vxlanpool"),
	}
	if err:=  command.InitPool(pool); err  != nil {
		fmt.Println("Init net pool error,%s",err)
		os.Exit(1)		
	}
	command.InitConfig(c.Bool("debug"), c.Bool("turnofffv"), c.String("etcd"), c.String("nic"),
					   c.String("nonatpool"),c.String("natpool"), c.String("vlanpool"),
					   c.String("vxlanpool"), c.String("flatstartip"), c.String("flatendip"), c.String("etcdprefix"))
	cha := make(chan int)
	go startupNetwork()
	go startupIpam()
	<- cha
	<- cha
}


func startupNetwork() {
	driver := qingluan.NewQingluanNet()
	headler := network.NewHandler(driver)
	fmt.Println(headler.ServeUnix(driver.Name, 0))
}

func startupIpam() {
	driver := ipamdriver.NewQingluanIpam()
	headler := ipamapi.NewHandler(driver)
	fmt.Println(headler.ServeUnix(driver.Name, 0))
}